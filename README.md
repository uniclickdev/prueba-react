# Carrito de compras


El objetivo del reto es crear un carrito de compras.

![Preview](./images/sketch.png)


### Requisitos que debe cumplir el proyecto
*  Al hacer clic en el botón "Agregar", el producto seleccionado se agrega al carrito
* El carrito contiene una lista de todos los productos agregados y el monto total
* Una vez agregado, el producto se puede eliminar del carrito
* Se puede agregar un producto al carrito más de una vez, en cuyo caso es necesario escribir cuántos productos hay en el carrito y su cantidad total individualmente

### Reglas 🧐
* Utilizar la lista de productos incluidas en el archivo [`products.json`](https://bitbucket.org/uniclickdev/prueba-react/src/master/products.json)
* El proyecto debe estar hecho en Reactjs
* Preste especial atención a la calidad del código, el formato y las mejores prácticas.
* Te brindamos (4hrs) para realizar el entregable

### Entregable ✔
* Puede enviar un enlace de repositorio que debamos clonar con las respectivas instrucciones
* Como segunda opción, puede enviar los archivos en un wetransfer: https://wetransfer.com/
* Cualquiera de ambos métodos, el enlace debe ser enviado a clau@ludik.pe y consultar si lo hemos recibido correctamente (se verificará hora de recepción)

### Puntos extra 🌟

* Características sorprendentes que no esperamos obtener  😉
* Se evaluará el tiempo de envío de la prueba VS el tiempo en que nos envías tu entregable 🕒
* Sugerimos enviar un readme con instrucciones de cómo levantar tu proyecto

_____

**¡Éxitos y cualquier duda, siéntete libre de escribirnos!** 💫

* Claudia Urcia Castillo (Jefe de desarrollo)  - clau@ludik.pe
* Roberto Jiménez (Administración)    - roberto@ludik.pe
